﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour {

    public TurretData laserTurretData;
    public TurretData missileTurretData;
    public TurretData standardTurretData;

    //表示当前选择的炮台（要建造的炮台）
    private TurretData selectedTurretData;

    public Text moneyText;

    private int money = 1000;

    void MoneyChange(int change)
    {
        money += change;
        moneyText.text = "Total:￥" + money;
    }
    
    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(!EventSystem.current.IsPointerOverGameObject())
            {
                //开发炮台的建造
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                bool isCollider=Physics.Raycast(ray,out hit, 1000, LayerMask.GetMask("MapCube"));
                if(isCollider)
                {
                    MapCube mapCube = hit.collider.GetComponent<MapCube>();
                    //得到点击的mapCube
                    if(mapCube.turretGo == null)
                    {
                        //可以创建
                        if(money>selectedTurretData.cost)
                        {
                            MoneyChange (-selectedTurretData.cost);
                            mapCube.BuildTurret(selectedTurretData.turretPrefab);
                        }
                        else
                        {
                            //TODO 提示钱不够
                        }
                    }
                    else
                    {
                        //TODO 升级处理
                    }
                }
            }
        }
    }

    public void OnLaserSelected(bool isOn)
    {
        if(isOn)
        {
            selectedTurretData = laserTurretData;
        }
    }

    private void OnMissileSelected(bool isOn)
    {
        if(isOn)
        {
            selectedTurretData = missileTurretData;
        }
    }

    public void OnStandardSelected(bool isOn)
    {
       if(isOn)
        {
            selectedTurretData = standardTurretData ;
        }
    }
}