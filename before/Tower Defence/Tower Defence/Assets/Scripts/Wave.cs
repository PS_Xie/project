﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//仅仅保存每一波敌人的属性
[System.Serializable]
public class Wave {
    public GameObject enemyPrefab;
    public int counter;
    public float rate;
}
