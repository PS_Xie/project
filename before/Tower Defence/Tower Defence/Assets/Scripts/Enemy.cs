﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private Transform[] positions;
    private int index = 0;
    public float speed;

	// Use this for initialization
	void Start () {
        positions = WayController.positions;
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    private void Move()
    {
        if(index >positions .Length-1 )
        {
            ReachDestination();
            return;
        }
        else
        {
            transform.Translate((positions[index].position - transform.position).normalized * Time.deltaTime * speed);
            if (Vector3.Distance(positions[index].position, transform.position) < 0.2f)
            {
                index++;
            }
        }
    }

    void ReachDestination()
    {
        GameObject.Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        EnemyCreater.countEnemyAlive--;
    }
}
