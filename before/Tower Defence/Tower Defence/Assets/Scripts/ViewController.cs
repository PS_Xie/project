﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewController : MonoBehaviour {

    public float speed;
    public float mousespeed;

	
	// Update is called once per frame
	void LateUpdate () {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        float m = Input.GetAxis("Mouse ScrollWheel");
        transform.Translate(new Vector3(h * speed, m * mousespeed, v * speed) *Time.deltaTime,Space.World);
    }
}
